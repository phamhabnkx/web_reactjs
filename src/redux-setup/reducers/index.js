import {combineReducers} from "redux"
import cartReducers from './cart'
import categoryReducers from './category'

export default combineReducers({
    Cart: cartReducers,
    Category: categoryReducers
})