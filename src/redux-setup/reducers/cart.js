import {actiontype} from "../../shared/constants"
const initState ={
    data:[
       
    ]
}
export default (state=initState, action) =>{
    switch(action.type){
        case actiontype.ADD_TO_CART:
            return _handleAddToCart(state,action.payload)
        case actiontype.UPDATE_QUANTITY_CART :
            return _handleUpdateQuantityCart(state,action.payload)
        case actiontype.DELETE_PRODUCT_CART:
            const newCart =state.data.filter((item)=>item.id !== action.payload.id)
            return {...state, data:newCart }
        case actiontype.DELETE_ALL_CART:
            return initState
        default:
            return state
    }
}
const _handleUpdateQuantityCart =(state,payload)=>{
    console.log("_handleUpdateQuantityCart -> payload", payload)
    const oldData = state.data
    console.log("_handleUpdateQuantityCart -> oldData", oldData)
    
    const newData = oldData.map((item)=>{
        console.log(item.id)
        if(item.id === payload.name){
            item.quantity = parseInt(payload.value)           
        }
        return item
    })
   
    return {
        ...state,
        data:newData

    }
}
const _handleAddToCart = (state,payload)=>{
    const oldData = state.data
    let isExists = false// bien nay la san pham da ton tai
    const newData = oldData.map((item)=>{
        if(!isExists && item.id === payload.id){
            item.quantity += payload.quantity
            isExists = true
        }
        return item
    })
    if(!isExists){
        newData.push(payload)
    }
    // localStorage.setItem("test",JSON.stringify(newData))
    return {
        ...state,
        data:newData

    }
}