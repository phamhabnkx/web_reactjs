import {createStore} from "redux"
import reducers from "./reducers"
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
const persistConfig ={
    key:"web",
    storage
}
const persistReducerConfig = persistReducer(persistConfig,reducers)
const store = createStore(persistReducerConfig,composeWithDevTools())
persistStore(store)
export default store