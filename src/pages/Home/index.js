import React from "react"
import Home from "./Home"
import {getProducts} from "../../service/server"
// or import * as A from "../../service/server" day laf export khac defaults

export default class HomeContainer extends React.Component {
    constructor(props){
        super(props)
        this.state ={
            newProducts :[],
            featureProducts:[]
        }
    }

   async componentDidMount(){
        //{data} la Destructuring_assignment , rest_parameters, Spread_syntax .tu dong map data
        // axios.get("http://domainforoffer.com/get-products").then(({ data })=>{
        //     console.log(data)
        // })
        // do phần product nó thay đổi liên tục nên ko cho vào redux store
        try{
            const newProducts = await getProducts({params:{limit:6}}).then(({data})=>{
                console.log(data.data)
                return data.data.docs;
                
             })
            const featureProducts = await getProducts({params:{ isFeatured:true,limit:6}}).then(({data})=>data.data.docs)
            this.setState({
                newProducts:newProducts,
                featureProducts:featureProducts,
     
            })
        }catch(e){
            console.log(e)
        }
      
    }
    _extract =()=>({
        ...this.state,
    })
    // hoac
    // _extract =()=>({
    //      newProducts,
    //       featureProducts,
    // })
    render(){
        return <Home {...this._extract()}/>
    }
}
