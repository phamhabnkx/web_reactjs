import React from "react"
import {
  ProductItem,
  ProductItemSkeleton} from '../../components'

export default class Home extends React.Component {
    _renderProducts =(products)=>{
      return products.map((product)=>{
          return <ProductItem key={product._id} item ={product}/>
      })
    }
    
    render() {
      
      const {newProducts, featureProducts} = this.props
     
        return (
            <>
                  {/*	Feature Product	*/}
                  <div className="products">
                    <h3>Sản phẩm nổi bật</h3>
                    <div className="product-list card-deck">
                      {this._renderProducts(featureProducts)}
                      
                    </div>
                   
                  </div>
                  {/*	End Feature Product	*/}
                  {/*	Latest Product	*/}
                  <div className="products">
                    <h3>Sản phẩm mới</h3>
                    <div className="product-list card-deck">
                    
                        {newProducts.length? this._renderProducts(newProducts):<ProductItemSkeleton loop={6}/>}
                      
                    </div>
                   
                  </div>
                  
            </>
        );
    }
}
