import React from "react"
import Category from "./Category"
import _ from "lodash"

import { getProducts,getCategories,getDetailCategories,getCategorybyProducts } from "../../service/server"
export default class CategoryContainer extends React.Component {
    state={
        products:[],
        name:null,
        loading:false
       
    }
    _exTract =()=>({
       ...this.state
    })
    async componentDidMount(){
        this.setState({
            loading:true
        })
        const id = _.get(this.props.match,"params.id")
        console.log("CategoryContainer -> componentDidMount -> id", id)
        const name = await getDetailCategories(id).then(({data})=>{
            return data.data
        })
        const products = await getCategorybyProducts(id,{params:{limit:12}}).then(({data})=>{
            return data.data.docs
        })
        this.setState({
            name,
            products,
            loading:false
           
        })
    }
    async componentDidUpdate(prevProps, prevState){
        const id = _.get(this.props,"match.params.id")
        const idOld = _.get(prevProps,"match.params.id")
       
        if(id !== idOld){
            this.setState({
                loading:true
            })
            const name = await getDetailCategories(id).then(({data})=>{
                return data.data
            })
            const products = await getCategorybyProducts(id,{params:{limit:12}}).then(({data})=>{
                return data.data.docs
            })
            
            
            this.setState({
                name,
                products,
                loading:false
               
            })
            
        }
    }
    render() {
        return <Category {...this._exTract()}/>
    }
}
