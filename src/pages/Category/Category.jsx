import React from "react"
import ReactPaginate from 'react-paginate'
import {
    ProductItem,
    ProductItemSkeleton} from "../../components"
import {
    BrowserRouter,
    Redirect,
    Route,
    Link,
    NavLink, 
    Switch} from 'react-router-dom'
import {getImageUrl, toslug} from "../../shared/utils"
export default class Category extends React.Component {
    render() {
        const {products,q,handlePageClick,loading} = this.props

        return (
            <div>
            
                <div className="products">
                    <div className="product-list card-deck">
                        {(!loading&&products && products.length &&
                            products.map(
                                (product) => (
                                    <ProductItem key={product._id} item={product} />
                                  )))||<ProductItemSkeleton loop={12}/>
                        }
                    </div>
                    
                   
                  
                </div>
                {/*	End List Product	*/}
                {/* <div id="pagination">
                    <ul className="pagination">
                    <li className="page-item"><a className="page-link" href="#">Trang trước</a></li>
                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                    <li className="page-item"><a className="page-link" href="#">Trang sau</a></li>
                    </ul> 
                </div> */}
                <form>
                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.props.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={6}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </form>
                
              

            </div>
        );
    }
}
