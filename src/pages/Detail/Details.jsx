import React from "react"
import {
    getImageUrl, 
    toslug} from "../../shared/utils"
import {
    BrowserRouter,
    Redirect,
    Route,
    Link,
    NavLink,
    Switch} from 'react-router-dom'
import { Notification } from "react-pnotify";
export default class Details extends React.Component {
    _stock = (is_stock)=>{
        if(is_stock === true){
            return <li id="status">Còn hàng </li>
        }else{
            return <li style={{color:"red"}}>Hết hàng</li>
        }
    }
    _renderProductsDetails =(products,onClickAddToCart)=>{ 
        // console.log("stock",products.is_stock)
            return (
                <>
                    <div id="product-head" className="row">
                            <div id="product-img" className="col-lg-6 col-md-6 col-sm-12">
                                <img src={products && getImageUrl(products.image)} />
                            </div>
                            <div id="product-details" className="col-lg-6 col-md-6 col-sm-12">
                                <h1>{products.name}</h1>
                                <ul>
                                <li><span>Bảo hành:</span> 12 Tháng</li>
                                <li><span>Đi kèm:</span> {products.accessories}</li>
                                <li><span>Tình trạng:</span> {products.status}</li>
                                <li><span>Khuyến Mại:</span> {products.promotion}</li>
                                <li id="price">Giá Bán (chưa bao gồm VAT)</li>
                                <li id="price-number">{products.price}</li> 
                                   {this._stock(products.is_stock)}
                                
                                </ul>
                                <div id="add-cart">
                                    {
                                       ( products &&products.is_stock &&<a href="#" onClick={(e)=>onClickAddToCart(e,products)}>Mua ngay</a> )|| null
                                    }
                                    {/* hoac {( products?.is_stock?.abc &&<a href="#">Mua ngay</a> )|| null} */}
                                    
                                </div>
                            </div>
                        </div>
                        <div id="product-body" className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12">
                                <h3>Đánh giá về iPhone X 64GB</h3>
                                <p>
                                 {products.details}
                                </p>
                            </div>
                        </div>
                </>
            )
      }
    render() {
        const {
            productDetails,
            comments,
            inputs,
            onChangeInput,
            onSubmitForm,
            onClickAddToCart,
            notifications}= this.props
       
        // console.log("Details -> render -> comments", comments)
      
        return (
            <>
            <div>
                <div id="product">
                    {this._renderProductsDetails(productDetails,onClickAddToCart)}
                    {/*	Comment	*/}
                    
                    <div id="comment" className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12">
                            <h3>Bình luận sản phẩm</h3>
                            <form method="post">
                            <div className="form-group">
                                <label>Tên:</label>
                                <input 
                                    name="name" 
                                    required type="text" 
                                    className="form-control"
                                    value={inputs.name}
                                    onChange={onChangeInput}
                                />
                            </div>
                            <div className="form-group">
                                <label>Email:</label>
                                <input 
                                    name="email" 
                                    required type="email" 
                                    className="form-control" 
                                    id="pwd" 
                                    value={inputs.email}
                                    onChange={onChangeInput}
                                />
                            </div>
                            <div className="form-group">
                                <label>Nội dung:</label>
                                <textarea 
                                    name="content" 
                                    required rows={8} 
                                    className="form-control" 
                                    defaultValue={""}
                                    onChange={onChangeInput}

                                     />     
                            </div>
                            <button type="submit" onClick={onSubmitForm} name="sbm" className="btn btn-primary">Gửi</button>
                            </form> 
                        </div>
                    </div>
                    {/*	End Comment	*/}  
                    {/*	Comments List	*/}
                    {comments&& comments.length &&
                        comments.map((comment)=>
                            <div key={comment._id} id="comments-list" className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12">
                                    <div className="comment-item">
                                    <ul>
                                        <li><b>{comment.name}</b></li>
                                        <li>
                                            {comment.data}
                                        </li>
                                        <li>
                                        <p>
                                            {comment.content}
                                        </p>
                                        </li>
                                    </ul>
                                    </div>
                                    
                                </div>
                            </div>
                        )}
                    
                    {/*	End Comments List	*/}
                </div>
                {/*	End Product	*/} 
                    <div id="pagination">
                        <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Trang trước</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">Trang sau</a></li>
                        </ul> 
                    </div>
                </div>
                {(notifications && <Notification {...notifications} />) || null}
            </>
        );
    }
}
