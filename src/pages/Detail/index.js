import React from "react"
import Details from "./Details"
import _ from "lodash"
import {
    getProductDetails,
    getCommentProduct,
    createComment} from "../../service/server"
import {connect } from "react-redux"
import {actiontype} from "../../shared/constants"

const inputDefault ={
    name:"",
    email:"",
    content:""
}
var timeOut = null;
 class DetailsContainer extends React.Component {
    constructor(props){
        super(props)
        this.state={
            productDetails:{},
            comments:[],
            inputs:{},

            notifications:null
            
          
            
        }
        
    }
    _onChangeInput = (e)=>{
        
        const {name,value} = e.target
        this.setState ({
            inputs:{...this.state.inputs,[name]:value}
        })
    }
    _onSubmitForm = async (e)=>{
        e.preventDefault()
        const id = _.get(this.props.match,"params.id")
        const {inputs} = this.state
        // await createComment({productId:id,...inputs})
        await createComment(id, inputs);
        const comments = await getCommentProduct(id,{}).then(({data})=>data.data.docs)
        this.setState({
            comments,
            inputs:inputDefault
  
         })

    }
    
    _onClickAddToCart =(e,products)=>{
        if (timeOut) return
        e.preventDefault()
        console.log(this.props)
        this.props.addToCart({
            id:products._id,
            price :products.price, 
            name:products.name,
            image:products.image ,
            quantity:1
        })
        this.setState({
            notifications: {
              type: "success",
              title: "Thanh cong",
              text: "San pham duoc them vao gio hang",
              animateIn: "zoomInLeft",
              animateOut: "zoomOutRight",
              hide: true,
              nonblock: true,
            }
        })
        timeOut = setTimeout(() => {
            this.setState({
              notifications: null,
            })
            timeOut = null
          }, 2000)
            console.log("DetailsContainer -> timeOut -> timeOut", timeOut)
          


    }
    async componentDidMount(){
        const id = _.get(this.props.match,"params.id")
        console.log("DetailsContainer -> componentDidMount -> id", id)
        
        try{
            const productDetails = await getProductDetails(id).then(({data})=>{    
                return data.data
            })
            
            const comments = await getCommentProduct(id,{}).then(({data})=>data.data.docs)
            console.log("DetailsContainer -> componentDidMount -> comments", comments)
            // const data = await Promise.allSettled([
            //         getProductDetails(id),
            //         getCommentProduct(id)
            // ])
            // Promise.all neeus moọt cái lỗi thì ko cho ra j cả .nên dùng Promise.Allset
            // console.log("DetailsContainer -> componentDidMount -> data", data)
            this.setState({
               productDetails,
               comments
     
            })
           //console.log(this.state.productDetails)
        }catch(e){
            console.log(e)
        }
    }
    _extractDetails =()=>({
        ...this.state,
        onChangeInput:this._onChangeInput,
        onSubmitForm:this._onSubmitForm,
        onClickAddToCart:this._onClickAddToCart
       
    })
    render() {
        console.log("inputs", this.state.inputs)
        // console.log(this.props.history.location.state)
        //console.log(this.props.match.params.id)
        return <Details {...this._extractDetails()}/>
    }
}
const mapDistpatchToprops = (dispatch) =>{
    return {
        addToCart: (item)=>dispatch({
            type:actiontype.ADD_TO_CART,
            payload: item,
        })
    }
}
export default connect(null,mapDistpatchToprops)(DetailsContainer)
//npm i react-debounce giup trong mot khoang thoi gian nao do ms cap nhat state tranhs tai trang nhieu lan
