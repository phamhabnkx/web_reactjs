import React from "react"
import Search from "./Search"
import {getProducts} from "../../service/server"
import ReactPaginate from 'react-paginate'
export default class SearchContainer extends React.Component {
    state ={
        products:[],
        q:"",

        offset: 0,
        data: [],
        perPage: 3,
        currentPage: 0
    }
   
    receivedData (){
        const searchParams = new URLSearchParams(this.props.location.search)
        const q = searchParams.get("q")
       
        getProducts({params :{name:q}}).then(({data})=>{
            const getData = data.data.docs
            const slice= getData.slice(this.state.offset, this.state.offset +this.state.perPage)
            console.log("SearchContainer -> componentDidMount -> slice", slice)
            console.log(Math.ceil(getData.length / this.state.perPage))
            this.setState({
                products:slice,
                q:q,
                pageCount: Math.ceil(getData.length / this.state.perPage),
            })
        })
    }
    __handlePageClick = (e) => {
        console.log("e",e)
        const selectedPage = e.selected;
        console.log("_handlePageClick -> selectedPage", selectedPage)
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.receivedData()
        });

    };
    componentDidMount(){
      
        //console.log(q)
        this.receivedData()
    }
    componentDidUpdate(prevProps, prevState){
        // console.log(prevProps)
        // const searchParams = new URLSearchParams(this.props.location.search)
        // const q = searchParams.get("q")
        // console.log(q)
        // if(q !== this.state.q){
        //     getProducts({params :{name:q,limit : 6}}).then(({data})=>{
        //         this.setState({
        //             products:data.data.docs,
        //             q:q
        //         })
        //     }) 
        // }
        if(prevProps.location.search !== this.props.location.search){
            this.receivedData()
        }
    }
    _extract =()=>({
        ...this.state,
        handlePageClick:this.__handlePageClick,
    })
    render() {
        return <Search {...this._extract()}/>
    }
}
