import React from "react"
import {getImageUrl, toslug} from "../../shared/utils"
import ReactPaginate from 'react-paginate'
import {BrowserRouter,Redirect,Route,Link,NavLink, Switch} from 'react-router-dom'
export default class Search extends React.PureComponent {
    
    render() {
        const {products,q,handlePageClick} = this.props
        console.log("Search -> render -> handlePageClick", handlePageClick)
        return (
            <div>
            
                <div className="products">
                    <div id="search-result">Kết quả tìm kiếm với sản phẩm <span>{q}</span></div>
                    <div className="product-list card-deck">
                        {products && products.length &&
                            products.map((product)=>
                                    <div className="product-item card text-center">
                                        <a href="#"><img src={getImageUrl(product.image)}/></a>
                                        <h4><Link to={{
                                                    pathname:`/details/${toslug(product.name)}.${product._id}.html`,
                                                    state:product._id
                                                }} >{product.name}</Link></h4>
                                        <p>Giá Bán: <span>{product.price}</span></p>
                                    </div> 
                            )
                        }
                    </div>
                    
                   
                  
                </div>
                {/*	End List Product	*/}
                {/* <div id="pagination">
                    <ul className="pagination">
                    <li className="page-item"><a className="page-link" href="#">Trang trước</a></li>
                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                    <li className="page-item"><a className="page-link" href="#">Trang sau</a></li>
                    </ul> 
                </div> */}
                <form>
                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.props.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={6}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
                </form>
                
              

            </div>
        );
    }
}
