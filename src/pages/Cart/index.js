/* eslint-disable no-restricted-globals*/
import React from "react"
import Cart from "./Cart"
import {connect} from "react-redux"
import {actiontype} from "../../shared/constants"
 class CartContainer extends React.Component {

    _extract =()=>{
        const {data} = this.props
        return{
            data:data,
            totalPrice:data.reduce((a,c)=>a+c.quantity*c.price,0),
            onChangeQuantity :this._onChangeQuantity,
            onClickProductCart:this._onClickProductCart,
            onClickDelAllCart:this._onClickDeleteAllCart
        
        }
        
    }
    _onClickProductCart =(e,id)=>{
        e.preventDefault()
        const isConfirm =  confirm("you want to delete product");
        return isConfirm? this.props.deleteProductCart({id}):false
    }
    _onClickDeleteAllCart =(e)=>{
        e.preventDefault()
        const isConfirm =  confirm("you want to delete product");
        return isConfirm? this.props.deleteAllCart():false
    }
    _onChangeQuantity = (e)=>{
        e.preventDefault()
        const {name,value } = e.target
        if(parseInt(value)<1){
           const isConfirm =  confirm("you want to delete product");
           return isConfirm? this.props.deleteProductCart({id:name}):false
        }
        this.props.updateAddToCart({name,value})
    }
    render() {
        
        return <Cart {...this._extract()}/>
        
    }
}
const mapStateToProps = (state)=>{
    return{
        data:state.Cart.data

    }
}
const mapDispatchToProps =(dispatch)=>{
    return{
        updateAddToCart: (item)=>dispatch({
            type:actiontype.UPDATE_QUANTITY_CART,
            payload: item,
        }),
        deleteProductCart: (item)=>dispatch({
            type:actiontype.DELETE_PRODUCT_CART,
            payload: item,
        }),
        deleteAllCart: ()=>dispatch({
            type:actiontype.DELETE_ALL_CART,
           
        }),
    }
    
}
export default connect(mapStateToProps,mapDispatchToProps)(CartContainer)
