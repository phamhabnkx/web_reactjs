import React from "react"
import {getImageUrl, toslug,formatPrice} from "../../shared/utils"
export default class Cart extends React.Component {
    render() {
        const {data,totalPrice,onChangeQuantity,onClickProductCart,onClickDelAllCart} = this.props
        let total = 0;
        return (
            <>
                <div id="my-cart">
                <div className="row">
                    <div className="cart-nav-item col-lg-7 col-md-7 col-sm-12">Thông tin sản phẩm</div> 
                    <div className="cart-nav-item col-lg-2 col-md-2 col-sm-12">Tùy chọn</div> 
                    <div className="cart-nav-item col-lg-3 col-md-3 col-sm-12">Giá</div>    
                </div>  
                <form method="post">
                {
                            data.map((item)=>
                                <div key={item.id} className="cart-item row">
                       
                                    <div className="cart-thumb col-lg-7 col-md-7 col-sm-12">
                                        <img src={item && getImageUrl(item.image)} />
                                        <h4>{item.name}</h4>
                                    </div> 
                                    <div className="cart-quantity col-lg-2 col-md-2 col-sm-12">
                                        <input 
                                            type="number" 
                                            name={item.id}
                                            onChange={onChangeQuantity}
                                            id="quantity" 
                                            className="form-control form-blue quantity" 
                                            value={item.quantity} defaultValue={0} 
                                           
                                        />
                                    </div> 
                                    <div className="cart-price col-lg-3 col-md-3 col-sm-12"><b>{formatPrice(item.price)}</b><a href="#" onClick={(e)=>onClickProductCart(e,item.id)}>Xóa</a></div>    
                                </div>  
                            )
                        }
                    
                   
                    
                    
                    
                    <div className="row">
                    <div className="cart-thumb col-lg-7 col-md-7 col-sm-12">
                        <a id="update-cart" className="btn btn-success" onClick={(e)=>onClickDelAllCart(e)} name="sbm">Xóa toàn bộ giỏ hàng</a>	
                    </div> 
                    <div className="cart-total col-lg-2 col-md-2 col-sm-12"><b>Tổng cộng:</b></div> 
                    <div className="cart-price col-lg-3 col-md-3 col-sm-12">
                        <b>
                            {formatPrice(totalPrice)}
                        </b>
                    </div>
                    </div>
                </form>
                </div>

            </>
        );
    }
}
