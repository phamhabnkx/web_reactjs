import React from 'react';
// pureComponent khác là nó kiểm tra xem state , props có thay đổi hay không mới render
import {Header,Menu,SideBar,Slider, Footer } from './components/layouts'
import {BrowserRouter} from "react-router-dom"
import AppRouter from './routers'
import {getCategories} from './service/server'
import {Provider} from "react-redux"
import store from "./redux-setup/store"

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state={
      time:0,
      categories:[],

    }
  }
 async componentDidMount(){
    // setInterval(() => {
    //   this.setState({
       
    //   })
    // }, 1000);
    await getCategories().then(({data})=>{
      this.setState({
        categories:data.data.docs
      })
    })
  //  const data =  JSON.parse(localStorage.getItem("test"))
  //  if(data.length){
  //     store.dispatch({
  //       type: "ADD_TO_CART",
  //       payload: data[0]
  //     })
  //  }
  


  }
  render() {
  //  console.log($)
    return (
      <Provider store={store}>

      
      <BrowserRouter>  
     
        <div>
          {/* header */}
          <Header/>
          {/*	Body	*/}
          <div id="body">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <nav>
                    {/*  menu */}
                    <Menu categories={this.state.categories}/>
                  </nav>
                </div>
              </div>
              <div className="row">
                <div id="main" className="col-lg-8 col-md-12 col-sm-12">
                  {/*	Slider	*/}
                  <Slider/>
                
                <AppRouter/>
                  
                </div>
                {/* sidebar */}

                <SideBar/>
              </div>
            </div>
          </div>
          {/*	End Body	*/}
          <Footer/>
          {/*	End Footer	*/}
        </div>

      </BrowserRouter>
      </Provider>
    );
  }
}

