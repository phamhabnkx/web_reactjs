import axios from 'axios'
import ApiConfig from "../config/api"
// custom duong dan tu dong map axios
const Http = axios.create({
    baseURL: ApiConfig.BASE_URL_API,
})
export default Http  