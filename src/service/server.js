
import Http from "./Http"
// cai extension turbo console.log
export const getProducts = (config={})=>{
    return Http.get("/products",config)

}
export const getProductDetails =(id,config={})=>{
    return Http.get(`/products/${id}`, config)
}
export const getCommentProduct =(id,config)=>{
    return Http.get(`/products/${id}/comments`, config)
}

export const createComment = (id,data={},config={})=>{
    return Http.post(`/products/${id}/comments`, data, config)
}
export const getCategories = (config={})=>{
    return Http.get(`/categories/`,config)
}
export const getDetailCategories = (id, config={})=>{
    return Http.get(`/categories/${id}`,config)

}
export const getCategorybyProducts = (id, config={})=>{
    return Http.get(`/categories/${id}/products`,config)
    
}