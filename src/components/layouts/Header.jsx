import React from "react"
import logo from "../../assets/images/logo.png"
import { withRouter } from "react-router-dom"
import {connect } from "react-redux"
import {BrowserRouter,Redirect,Route,Link,NavLink, Switch} from 'react-router-dom'
// khi component ko nam trong router thif vc truyen this.props can su dung withRoute
class Header extends React.Component {
    constructor(props){
        super(props)
        this.state ={
            keyWord : null
        }
    }
    onSubmitSearch = (e) => {
        e.preventDefault()
        const {history} = this.props
        history.push(`/search?q=${this.state.keyWord}`)
        
    }
    render() {
        console.log(this.props)
        const {totalItemCart} = this.props
        return (
            <>
                <div id="header">
                    <div className="container">
                    <div className="row">
                        <div id="logo" className="col-lg-3 col-md-3 col-sm-12">
                        <h1><Link to={"/"} href="#"><img className="img-fluid" src={logo} /></Link></h1>
                        </div>
                        <div id="search" className="col-lg-6 col-md-6 col-sm-12">
                        <form className="form-inline">
                            <input 
                                className="form-control mt-3" 
                                type="search" 
                                placeholder="Tìm kiếm" 
                                aria-label="Search"
                                onChange={
                                    (e)=>{
                                        this.setState({keyWord:e.target.value})
                                    }
                                } 
                                value ={this.state.keyWord}
                            />
                            <button className="btn btn-danger mt-3" type="submit" onClick={this.onSubmitSearch}>Tìm kiếm</button>
                        </form>
                        </div>
                        <div id="cart" className="col-lg-3 col-md-3 col-sm-12">
                            <Link to={"/cart"} className="mt-4 mr-2" href="#">giỏ hàng</Link><span className="mt-3">{totalItemCart}</span>
                        </div>
                    </div>
                    </div>
                    {/* Toggler/collapsibe Button */}
                    <button className="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#menu">
                    <span className="navbar-toggler-icon" />
                    </button>
                </div>
                {/*	End Header	*/}
            </>
        );
    }
}
const mapStateToProps = (state)=>{
    return {
        totalItemCart :state.Cart.data.reduce((a,c)=>a+c.quantity,0),
    }
}
export default connect(mapStateToProps)(withRouter(Header)) 
