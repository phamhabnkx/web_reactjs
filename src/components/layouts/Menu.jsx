import React from "react"
import {BrowserRouter,Redirect,Route,Link,NavLink, Switch} from 'react-router-dom'
export default class Menu extends React.Component {
    render() {
       const {categories} = this.props
        return (
            <div>
                <div id="menu" className="collapse navbar-collapse">
                      <ul>
                          {
                              categories.map((data)=>{
                                  return(
                                        <li key={data._id} className="menu-item"><Link to={`/category/${data._id}`}>{data.name}</Link></li>
                                    )
                              
                              })
                          }
                        
                      
                      </ul>
                    </div>
            </div>
        );
    }
}
