import React from "react"
import {getImageUrl, toslug,formatPrice} from "../shared/utils"
import {BrowserRouter,Redirect,Route,Link,NavLink, Switch} from 'react-router-dom'
// khi ko xu ly nhieu thi tach ra dung function component
// chungs ta se destrusting props ra {item1, item2}
class ProductItem extends React.PureComponent{
    render(){
        const {item} = this.props
        return (
            <>
                <div className="product-item card text-center">
                        <a href="#"><img src={getImageUrl(item.image)} /></a>
                        <h4><Link to={{
                            pathname:`/details/${toslug(item.name)}.${item._id}.html`,
                            state:item._id
                        }} >{item.name}</Link></h4>
                        <p>Giá Bán: <span>{formatPrice(item.price)} đ</span></p>
                      </div> 
            </>
        );
    }
}
// PureComponent tranh render lai nhieu lan
// function ProductItem ({item}) {
//         //console.log(toslug("/details"+toslug(item.name)+"."+item._id+".html"))
//         return (
//             <>
//                 <div className="product-item card text-center">
//                         <a href="#"><img src={getImageUrl(item.image)} /></a>
//                         <h4><Link to={{
//                             pathname:`/details/${toslug(item.name)}.${item._id}.html`,
//                             state:item._id
//                         }} >{item.name}</Link></h4>
//                         <p>Giá Bán: <span>{item.price} đ</span></p>
//                       </div> 
//             </>
//         );

// }
export default ProductItem
