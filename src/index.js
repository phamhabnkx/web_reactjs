import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App"
import "./assets/css/bootstrap.min.css"
import "./assets/css/cart.css"
import "./assets/css/category.css"
import "./assets/css/home.css"
import "./assets/css/product.css"
import "./assets/css/search.css"
import "./assets/css/success.css"

ReactDOM.render(
    <App />
,
  document.getElementById('root')
);


