import React from 'react'
import {Switch, Route, Redirect} from "react-router-dom"
import {HomePage, NotFoundPage, CartPage,CategoryPage,DetailsPage,OrderSuccessPage,SeachPage} from "../pages"
export default class AppRouter extends React.Component{
    render(){
        return (
            <Switch>
                <Route path="/" exact component={HomePage} />
                <Route path="/404" exact component={NotFoundPage} />
                <Route path="/cart" exact component={CartPage} />
                <Route path="/category/:id" exact component={CategoryPage} />
                <Route path="/details/:slug.:id.html" exact component={DetailsPage} />
                <Route path="/order-success" exact component={OrderSuccessPage} />
                <Route path="/search" exact component={SeachPage} />
                <Route path="*" render={()=><Redirect to="/404" />} />
            </Switch>
        )
        
    }
}